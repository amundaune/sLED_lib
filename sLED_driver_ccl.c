/*
 * sLED_driver_ccl.c
 *
 * Created: 01.03.2018 22:20:49
 * Author : Egil Rotevatn
 *
 * LED send data for xxn812yyyy type smart LEDs
 * Uses tinyAVR 0- and 1-series TCA+SPI+CCL for 0' and 1' timing
 */

#include <avr/io.h>
#include <stdint.h>
#include "sLED_driver.h"

#define SLED_TIMER_PERIOD           ((F_CPU/SLED_DRIVER_FREQ)-1)

void sLED_driver_init(void)
{
	PORTB.DIRSET = PIN2_bm; //?
	TCA0.SINGLE.PER = SLED_TIMER_PERIOD;
	TCA0.SINGLE.CMP2 = SLED_TIMER_PERIOD/2;
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP2EN_bm  | TCA_SINGLE_WGMODE_SINGLESLOPE_gc;
	TCA0.SINGLE.CTRLC = TCA_SINGLE_CMP2OV_bm; 

	PORTA.DIRSET = PIN1_bm | PIN3_bm; //?
	SPI0.CTRLA = SPI_ENABLE_bm |  SPI_MASTER_bm | SPI_PRESC_DIV16_gc;
	SPI0.CTRLB = SPI_MODE_0_gc | SPI_SSD_bm;

	PORTC.DIRSET = PIN1_bm;
	PORTMUX.CTRLA |= PORTMUX_LUT1_bm;
	CCL.TRUTH1 = 168; //?
	CCL.LUT1CTRLC = CCL_INSEL2_TCA0_gc;
	CCL.LUT1CTRLB = CCL_INSEL0_SPI0_gc | CCL_INSEL1_SPI0_gc ;
	CCL.LUT1CTRLA = CCL_ENABLE_bm | CCL_OUTEN_bm;
	CCL.CTRLA = CCL_ENABLE_bm | CCL_RUNSTDBY_bm;
}

void sLED_driver_senddata(single_led_t data)
{
	for (uint8_t i = 0; i < SLED_CHANNELS; i++)
	{
		// Composition of 24bit data:
		// G7 G6 G5 G4 G3 G2 G1 G0 R7 R6 R5 R4 R3 R2 R1 R0 B7 B6 B5 B4 B3 B2 B1 B0
		// Note: Follow the order of GRB to sent data and the high bit sent at first.
		// Clear the Write Collision flag, to allow writing
		SPI0.INTFLAGS = SPI0_INTFLAGS;

		// Reset TCA counter register to ensure the first rising edge of PWM is predictable
		TCA0.SINGLE.CNT = 0 /* Count: 0 */;

		// Start TCA
		TCA0.SINGLE.CTRLA = TCA_SPLIT_CLKSEL_DIV1_gc /* System Clock */
		| 1 << TCA_SPLIT_ENABLE_bp /* Module Enable: enabled */;

		// Start SPI by writing a byte to SPI data register
		SPI0.DATA = data.channel[SLED_CHANNELS-1-i];

		// Wait for transfer to complete
		while ((SPI0.INTFLAGS & SPI_RXCIF_bm) == 0) {
		}

		// Stop TCA
		TCA0.SINGLE.CTRLA = TCA_SPLIT_CLKSEL_DIV1_gc /* System Clock */
		| 0 << TCA_SPLIT_ENABLE_bp /* Module Enable: disabled */;
	}
}


/*********************************************************************************
Set single pixel (old)
*********************************************************************************/
void sLED_driver_set_single_led(uint8_t pixel, uint8_t pixels,
                                single_led_t
                                data)       //Set a single LED with rgb data. writes all other leds to 0.
{
	uint8_t i;
	single_led_t zero_data = {0};

	for(i = 0; i < pixel; i++) {
		sLED_driver_senddata(zero_data);
	}

	sLED_driver_senddata(data);

	for(i = (pixel + 1); i < pixels; i++) {
		sLED_driver_senddata(zero_data);
	}
}


/*********************************************************************************
Write function for writing contents of a data struct array out to led strip
*********************************************************************************/
void sLED_driver_configure_leds(single_led_t *ledarray_ptr,
                                uint8_t pixels)                                             //writes to n number of leds directly from given led array
{
	for(uint8_t i = 0; i < pixels; i++) {
		sLED_driver_senddata(ledarray_ptr[i]);
	}
}

void sLED_driver_configure_leds_2darray_oddflip(single_led_t *ledarray_ptr,
        uint8_t pixel_rows, uint8_t
        pixels_columns) //writes to x*y number of leds directly from given led array
{
	uint8_t j, i;
	for(i = 0; i < pixels_columns; i++) {
		if(i % 2) { //Every odd row flips direction
			for(j = 0; j < pixel_rows; j++) {
				sLED_driver_senddata(ledarray_ptr[(i + 1)*pixel_rows - 1 - j]);
			}
		} else {
			for(j = 0; j < pixel_rows; j++) {
				sLED_driver_senddata(ledarray_ptr[i * pixel_rows + j]);
			}
		}
	}

}

/*********************************************************************************
USART print function for printing led array data
*********************************************************************************/
void sLED_printf_all_leds(single_led_t *ledarray_ptr,
                          uint8_t pixels)                          //prints all led array data to usart
{
	for(uint8_t i = 0; i < pixels; i++) {               //print whole array
		//printf("LED[%d] data r, g, b, w: %d, %d, %d, %d\n\r",i, ledarray_ptr[i].red, ledarray_ptr[i].green, ledarray_ptr[i].blue, ledarray_ptr[i].white);
	}
}
