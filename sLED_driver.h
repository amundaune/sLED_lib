/*
 * sLED_driver.h
 *
 * Created: 08.09.2016 09:20:49
 * Author : Egil Rotevatn
 */ 

#ifndef _sLED_driver_H_
#define _sLED_driver_H_
enum {
	SLED_MASK_BLUE	=	(1 << 0),
	SLED_MASK_RED	=	(1 << 1),
	SLED_MASK_GREEN	=	(1 << 2),
	SLED_MASK_WHITE	=	(1 << 3),
	SLED_MASK_RGB	=	(SLED_MASK_RED | SLED_MASK_GREEN | SLED_MASK_BLUE),
	SLED_MASK_RGBW	=	(SLED_MASK_RGB | SLED_MASK_WHITE)
} sled_mask;

#define SLED_MASK_WARM		SLED_MASK_BLUE
#define SLED_MASK_AMBER		SLED_MASK_GREEN
#define SLED_MASK_COLD		SLED_MASK_RED
#define SLED_MASK_WWA		SLED_MASK_RGB

#ifdef SLED_DRIVER_RGB
	typedef union single_led_rgb
	{
		struct
		{
			uint8_t	blue;
			uint8_t red;
			uint8_t green;
		};
		uint8_t channel[3];
	}single_led_t;

enum {
	BLUE = 0,
	RED,
	GREEN,
	SLED_CHANNELS
} color_channels;

#elif defined (SLED_DRIVER_RGBW)
	typedef union single_led_rgbw
	{
		struct
		{
			uint8_t	white;
			uint8_t	blue;
			uint8_t red;
			uint8_t green;
		};
		uint8_t channel[4];
		uint32_t all;   //To avoid having to arrange data before sending, can also be used for bitlevel fun
	}single_led_t;

enum {
	WHITE = 0,
	BLUE,
	RED,
	GREEN,
	SLED_CHANNELS
} color_channels;
    
#elif defined (SLED_DRIVER_WWA)
	typedef union single_led_wwa
	{
		struct  
		{        
			uint8_t warm;
			uint8_t amber;
			uint8_t	cold;
		};
		struct  //Cheating to make functions compatible without needing to change name..
		{        
			uint8_t	blue;
			uint8_t red;
			uint8_t green;
		};
		uint8_t channel[3];
    }single_led_t;

enum {
	BLUE = 0,
	RED,
	GREEN,
	SLED_CHANNELS
} color_channels; 

enum {
	WARM = 0,
	AMBER,
	COLD,
} wwa_channels;
#else
	#error (Need to define SLED_DRIVER_RGB, SLED_DRIVER_RGBW or SLED_DRIVER_WWA)
#endif

#ifdef SLED_DRIVER_RGBW
	#define SLED_TIMER_BITS 32
	#define ALIGN_COLOR_DATA()		(data.all)
#else
	#define SLED_TIMER_BITS 24
	#define ALIGN_COLOR_DATA()		(((uint32_t) data.green<<24) | ((uint32_t) data.red  <<16) | ((uint32_t) data.blue  <<8))
#endif

/*********************************************************************************
Hardware driver
*********************************************************************************/
void sLED_driver_init(void);
void sLED_driver_senddata(single_led_t data);

/*********************************************************************************
Write function for setting single pixel (and clearing the rest) (old)
*********************************************************************************/
void sLED_driver_set_single_led(uint8_t pixel, uint8_t pixels, single_led_t data);

/*********************************************************************************
Write function for writing contents of a data struct array out to led strip
*********************************************************************************/
void sLED_driver_configure_leds(single_led_t *ledarray_ptr, uint8_t pixels);
void sLED_driver_configure_leds_2darray_oddflip(single_led_t *ledarray_ptr, uint8_t pixel_rows, uint8_t pixels_columns);

/*********************************************************************************
USART print function for printing led array data
*********************************************************************************/
void sLED_printf_all_leds (single_led_t *ledarray_ptr, uint8_t pixels);




#endif