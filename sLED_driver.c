/*
 * sLED_driver.c
 *
 * Created: 08.09.2016 09:20:49
 * Authors : Stig Larssen and Egil Rotevatn
 *
 * LED send data for xxn812yyyy type smart LEDs
 * Uses a mega324pb timer in PWM mode and compare matching to set 0' and 1' timing
 */

#include <avr/io.h>
#include <stdint.h>
#include "sLED_driver.h"

#define SLED_TIMER_PERIOD           ((F_CPU/SLED_DRIVER_FREQ)-1)    //register setting. F_CPU = 32MHz, 800kHz -> PER = 39

#define SLED_TIMER_1_HIGHTIME       660ul                           //600 ns. Dutycycle from datasheet timing, 0.6us high time, 0.6us low time (+-0.15us)
#define SLED_TIMER_0_HIGHTIME       360ul                           //300 ns. Dutycycle from datasheet timing, 0.3us high time, 0.9us low time (+-0.15us)
#define SLED_TIMER_1_DUTYCYCLE      ((SLED_TIMER_1_HIGHTIME*(F_CPU/1000000ul))/1000ul)  //register setting
#define SLED_TIMER_0_DUTYCYCLE      ((SLED_TIMER_0_HIGHTIME*(F_CPU/1000000ul))/1000ul)  //register setting

#define SLED_TIMER_RESET_TIME       80                              //us

#ifdef __AVR_ATmega324PB__
#define SLED_TIMER_1_BIT()          (OCR1A = SLED_TIMER_1_DUTYCYCLE)
#define SLED_TIMER_0_BIT()          (OCR1A = SLED_TIMER_0_DUTYCYCLE)
#elif defined (__AVR_ATxmega8E5__) || defined (__AVR_ATxmega16E5__) || defined (__AVR_ATxmega32E5__)
#define SLED_TIMER_1_BIT()          (TCC4.CCA = SLED_TIMER_1_DUTYCYCLE)
#define SLED_TIMER_0_BIT()          (TCC4.CCA = SLED_TIMER_0_DUTYCYCLE)
#elif defined (__AVR_ATxmega16A4U__) || defined (__AVR_ATxmega32A4U__) || defined (__AVR_ATxmega64A4U__) || defined (__AVR_ATxmega128A4U__) || \
              (__AVR_ATxmega16A4__) || defined (__AVR_ATxmega32A4__) || defined (__AVR_ATxmega64A4__) || defined (__AVR_ATxmega128A4__)
#define SLED_TIMER_1_BIT()          (TCD0.CCA = SLED_TIMER_1_DUTYCYCLE)
#define SLED_TIMER_0_BIT()          (TCD0.CCA = SLED_TIMER_0_DUTYCYCLE)
#endif

#ifdef SLED_DRIVER_RGBW
#define SLED_TIMER_BITS 32
#define ALIGN_COLOR_DATA()      (data.all) // (((uint32_t) data.green<<24) | ((uint32_t) data.red  <<16) | ((uint32_t) data.blue  <<8) | ((uint32_t) data.white))
#else
#define SLED_TIMER_BITS 24
#define ALIGN_COLOR_DATA()      (((uint32_t) data.green<<24) | ((uint32_t) data.red  <<16) | ((uint32_t) data.blue  <<8))
#endif

void sLED_driver_init(void)
{
#ifdef __AVR_ATmega324PB__
	//Do I need to set PD5 to output low?
	DDRD |= (1 << PIND5);

	//Enable TC0 to Fast PWM -> PD5 OCR1A and start it
	ICR1 = SLED_TIMER_PERIOD;
	TCCR1A = (1 << WGM11) | (0 << WGM10) | (1 << COM1A1) | (0 << COM1A0);
	TCCR1B = (1 << WGM13) | (1 << WGM12);

#elif defined (__AVR_ATxmega8E5__) || defined (__AVR_ATxmega16E5__) || defined (__AVR_ATxmega32E5__)
	PR.PRPC &= ~PR_TC4_bm;

	//Do I need to set pin to output low?
	PORTC.DIRSET = PIN0_bm;

	TCC4.PER = SLED_TIMER_PERIOD;  //Set PWM output frequency.
	TCC4.CTRLB = TC_WGMODE_SINGLESLOPE_gc; //Single slope PWM
	TCC4.CTRLE = TC_CCAMODE_COMP_gc;  //Output compare on CCA -> OC4A
#elif defined (__AVR_ATxmega16A4U__) || defined (__AVR_ATxmega32A4U__) || defined (__AVR_ATxmega64A4U__) || defined (__AVR_ATxmega128A4U__) || \
              (__AVR_ATxmega16A4__) || defined (__AVR_ATxmega32A4__) || defined (__AVR_ATxmega64A4__) || defined (__AVR_ATxmega128A4__)
	PR.PRPD &= ~PR_TC0_bm;

	//Do I need to set pin to output low?
	PORTD.DIRSET = PIN0_bm;

	TCD0.PER = SLED_TIMER_PERIOD;   //Set PWM output frequency.
	TCD0.CTRLB = TC_WGMODE_SS_gc | //Single slope PWM
	             TC0_CCAEN_bm;  //Output compare on CCA -> OC0A

#endif
}

void sLED_driver_senddata(single_led_t data)
{
	uint8_t pulse_cnt = 0;
	uint32_t colors = 0;

	//align color data in correct order
	colors = ALIGN_COLOR_DATA();

	//Set up first bit for appropriate timing value in compare match register
	if((colors & 0x80000000) == 0x80000000) {
		SLED_TIMER_1_BIT();
	} else {
		SLED_TIMER_0_BIT();
	}

#ifdef __AVR_ATmega324PB__
	//Pins do not toggle on first cycle so set count a bit high, but not too high
	TCNT1 = SLED_TIMER_PERIOD - 10;
	//Clear compare int flag first time
	TIFR1 = (1 << OCF1A);
	//Start timer
	TCCR1B |= (1 << CS10);
#elif defined (__AVR_ATxmega8E5__) || defined (__AVR_ATxmega16E5__) || defined (__AVR_ATxmega32E5__)
	TCC4.CNT = 0;
	TCC4.INTFLAGS = 0xff;   //Clear intflag first time
	TCC4.CTRLA = TC_CLKSEL_DIV1_gc;   //Start TCC4
#elif defined (__AVR_ATxmega16A4U__) || defined (__AVR_ATxmega32A4U__) || defined (__AVR_ATxmega64A4U__) || defined (__AVR_ATxmega128A4U__) || \
              (__AVR_ATxmega16A4__) || defined (__AVR_ATxmega32A4__) || defined (__AVR_ATxmega64A4__) || defined (__AVR_ATxmega128A4__)
	TCD0.CNT = 0;
	TCD0.INTFLAGS =
	    0xff;   //Clear intflag first time
	TCD0.CTRLA = TC_CLKSEL_DIV1_gc; //Start TCD0
#endif

	//start to shift out all bits using timer in PWM mode and compare match
	while(pulse_cnt < (SLED_TIMER_BITS - 1)) {
		pulse_cnt++;
		colors <<= 1;   //shift data one position left

#ifdef __AVR_ATmega324PB__
		while((TIFR1 & (1 << OCF1A)) ==
		        0) {}                       //wait for compare match interrupt flag
		TIFR1 = (1 <<
		         OCF1A);                                       //reset comp match int flag
#elif defined (__AVR_ATxmega8E5__) || defined (__AVR_ATxmega16E5__) || defined (__AVR_ATxmega32E5__)
		while((TCC4.INTFLAGS & TC4_CCAIF_bm) ==
		        0) {}                           //wait for compare match interrupt flag
		TCC4.INTFLAGS =
		    TC4_CCAIF_bm;                                           //reset comp match int flag
#elif defined (__AVR_ATxmega16A4U__) || defined (__AVR_ATxmega32A4U__) || defined (__AVR_ATxmega64A4U__) || defined (__AVR_ATxmega128A4U__) || \
              (__AVR_ATxmega16A4__) || defined (__AVR_ATxmega32A4__) || defined (__AVR_ATxmega64A4__) || defined (__AVR_ATxmega128A4__)
		while((TCD0.INTFLAGS & TC0_CCAIF_bm) ==
		        0) {}                           //wait for compare match interrupt flag
		TCD0.INTFLAGS =
		    TC0_CCAIF_bm;                                           //reset comp match int flag
#endif

		if((colors & 0x80000000) ==
		        0x80000000) {                   //Check next bit for appropriate timing value in compare match register
			SLED_TIMER_1_BIT();
		} else {
			SLED_TIMER_0_BIT();
		}
	}
#ifdef __AVR_ATmega324PB__
	while((TIFR1 & (1 << OCF1A)) ==
	        0) {}                           //wait for compare match interrupt flag for last bit
	TCCR1B &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));           //Stop timer
#elif defined (__AVR_ATxmega8E5__) || defined (__AVR_ATxmega16E5__) || defined (__AVR_ATxmega32E5__)
	while((TCC4.INTFLAGS & TC4_CCAIF_bm) ==
	        0) {}                               //wait for compare match interrupt flag - comment out if running at high freq?
	TCC4.INTFLAGS = TC4_OVFIF_bm;
	TCC4.CTRLA |=
	    TC4_UPSTOP_bm;                                                //Stop timer at next overflow
	while((TCC4.INTFLAGS & TC4_OVFIF_bm) ==
	        0) {}                               //Wait for last overflow
#elif defined (__AVR_ATxmega16A4U__) || defined (__AVR_ATxmega32A4U__) || defined (__AVR_ATxmega64A4U__) || defined (__AVR_ATxmega128A4U__) || \
              (__AVR_ATxmega16A4__) || defined (__AVR_ATxmega32A4__) || defined (__AVR_ATxmega64A4__) || defined (__AVR_ATxmega128A4__)
	while((TCD0.INTFLAGS & TC0_CCAIF_bm) ==
	        0) {}    //wait for compare match interrupt flag
	TCD0.CTRLA = 0;                            //Stop timer

#endif
}


/*********************************************************************************
Set single pixel (old)
*********************************************************************************/
void sLED_driver_set_single_led(uint8_t pixel, uint8_t pixels,
                                single_led_t
                                data)       //Set a single LED with rgb data. writes all other leds to 0.
{
	uint8_t i;
	single_led_t zero_data = {0};

	for(i = 0; i < pixel; i++) {
		sLED_driver_senddata(zero_data);
	}

	sLED_driver_senddata(data);

	for(i = (pixel + 1); i < pixels; i++) {
		sLED_driver_senddata(zero_data);
	}
}


/*********************************************************************************
Write function for writing contents of a data struct array out to led strip
*********************************************************************************/
void sLED_driver_configure_leds(single_led_t *ledarray_ptr,
                                uint8_t pixels)                                             //writes to n number of leds directly from given led array
{
	for(uint8_t i = 0; i < pixels; i++) {
		sLED_driver_senddata(ledarray_ptr[i]);
	}
}

void sLED_driver_configure_leds_2darray_oddflip(single_led_t *ledarray_ptr,
        uint8_t pixel_rows, uint8_t
        pixels_columns) //writes to x*y number of leds directly from given led array
{
	uint8_t j, i;
	for(i = 0; i < pixels_columns; i++) {
		if(i % 2) { //Every odd row flips direction
			for(j = 0; j < pixel_rows; j++) {
				sLED_driver_senddata(ledarray_ptr[(i + 1)*pixel_rows - 1 - j]);
			}
		} else {
			for(j = 0; j < pixel_rows; j++) {
				sLED_driver_senddata(ledarray_ptr[i * pixel_rows + j]);
			}
		}
	}

}

/*********************************************************************************
USART print function for printing led array data
*********************************************************************************/
void sLED_printf_all_leds(single_led_t *ledarray_ptr,
                          uint8_t pixels)                          //prints all led array data to usart
{
	for(uint8_t i = 0; i < pixels; i++) {               //print whole array
		//printf("LED[%d] data r, g, b, w: %d, %d, %d, %d\n\r",i, ledarray_ptr[i].red, ledarray_ptr[i].green, ledarray_ptr[i].blue, ledarray_ptr[i].white);
	}
}
