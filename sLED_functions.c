/*
 * sLED_driver.c
 *
 * Created: 08.09.2016 09:20:49
 * Authors : Stig Larssen and Egil Rotevatn
 *
 * LED send data for xxn812yyyy type smart LEDs
 * Uses a mega324pb timer in PWM mode and compare matching to set 0' and 1' timing
 */ 

#include <stdint.h>
#include <stdbool.h>
#include "sLED_functions.h"

/*********************************************************************************
Write function for updating contents of a data struct array
*********************************************************************************/
void sLED_array_update_pixel(led_config_t *led, uint8_t pixel)
{
	if (led->color_mask & SLED_MASK_BLUE)
	{
		led->array_ptr[pixel].blue = led->input_color.blue;
	}
	if (led->color_mask & SLED_MASK_RED)
	{
		led->array_ptr[pixel].red = led->input_color.red;
	}
	if (led->color_mask & SLED_MASK_GREEN)
	{
		led->array_ptr[pixel].green = led->input_color.green;
	}
	#ifdef SLED_DRIVER_RGBW
	if (led->color_mask & SLED_MASK_WHITE)
	{
		led->array_ptr[pixel].white = led->input_color.white;
	}
	#endif
}

void sLED_array_clear_pixel(led_config_t *led, uint8_t pixel)
{
	if (led->color_mask & SLED_MASK_BLUE)
	{
		led->array_ptr[pixel].blue = 0x00;
	}
	if (led->color_mask & SLED_MASK_RED)
	{
		led->array_ptr[pixel].red = 0x00;
	}
	if (led->color_mask & SLED_MASK_GREEN)
	{
		led->array_ptr[pixel].green = 0x00;
	}
	#ifdef SLED_DRIVER_RGBW
	if (led->color_mask & SLED_MASK_WHITE)
	{
		led->array_ptr[pixel].white = 0x00;
	}
	#endif
}

bool sLED_array_fade_pixel(led_config_t *led, uint8_t pixel)
{
	bool done = true;
	if (led->color_mask & SLED_MASK_BLUE)
	{
		if (led->array_ptr[pixel].blue < led->input_color.blue)
		{
			led->array_ptr[pixel].blue++;
			done = false;		
		} else if (led->array_ptr[pixel].blue > led->input_color.blue)
		{
			led->array_ptr[pixel].blue--;				
			done = false;
		}
	}
	if (led->color_mask & SLED_MASK_RED)
	{
		if (led->array_ptr[pixel].red < led->input_color.red)
		{
			led->array_ptr[pixel].red++;
			done = false;
		} else if (led->array_ptr[pixel].red > led->input_color.red)
		{
			led->array_ptr[pixel].red--;
			done = false;
		}
	}
	if (led->color_mask & SLED_MASK_GREEN)
	{
		if (led->array_ptr[pixel].green < led->input_color.green)
		{
			led->array_ptr[pixel].green++;
			done = false;
		} else if (led->array_ptr[pixel].green > led->input_color.green)
		{
			led->array_ptr[pixel].green--;
			done = false;
		}
	}
	#ifdef SLED_DRIVER_RGBW
	if (led->color_mask & SLED_MASK_WHITE)
	{
		if (led->array_ptr[pixel].white < led->input_color.white)
		{
			led->array_ptr[pixel].white++;
			done = false;
		} else if (led->array_ptr[pixel].white > led->input_color.white)
		{
			led->array_ptr[pixel].white--;
			done = false;
		}
	}
	#endif
	
	return done;
}

bool sLED_array_fade_off_pixel(led_config_t *led, uint8_t pixel)
{
	bool done = true;
	if (led->color_mask & SLED_MASK_BLUE)
	{
		if (led->array_ptr[pixel].blue) {
			led->array_ptr[pixel].blue--;
			done = false;
		}
	}
	if (led->color_mask & SLED_MASK_RED)
	{
		if (led->array_ptr[pixel].red) {
			led->array_ptr[pixel].red--;
			done = false;
		}
	}
	if (led->color_mask & SLED_MASK_GREEN)
	{
		if (led->array_ptr[pixel].green) {
			led->array_ptr[pixel].green--;
			done = false;
		}
	}
	#ifdef SLED_DRIVER_RGBW
	if (led->color_mask & SLED_MASK_WHITE)
	{
		if (led->array_ptr[pixel].white) {
			led->array_ptr[pixel].white--;
			done = false;
		}
	}
	#endif
	
	return done;
}


void sLED_array_update_all(led_config_t *led)
{
	for (uint8_t i=0; i<(led->array_columns*led->array_rows*led->arrays); i++)
	{
		sLED_array_update_pixel(led, i);
	}
}

void sLED_array_clear_all(led_config_t *led)
{
	for (uint8_t i=0; i<(led->array_columns*led->array_rows*led->arrays); i++)
	{
		sLED_array_clear_pixel(led, i);
	}
}

bool sLED_array_fade_all(led_config_t *led)
{
	bool done = true;

	for (uint8_t i=0; i<(led->array_columns*led->array_rows*led->arrays); i++)
	{
		if(!sLED_array_fade_pixel(led, i)) {
			done = false;
		}
	}
	
	return done;
}

bool sLED_array_fade_off_all(led_config_t *led)
{
	bool done = true;

	for (uint8_t i=0; i<(led->array_columns*led->array_rows*led->arrays); i++)
	{
		if(!sLED_array_fade_off_pixel(led, i)) {
			done = false;
		}
	}
	
	return done;
}

void sLED_array_clear_all_set_pixel(led_config_t *led, uint8_t pixel)
{
	sLED_array_clear_all(led);
	sLED_array_update_pixel(led, pixel);
}

void sLED_array_update_pixel_range(led_config_t *led, uint8_t startpixel, uint8_t pixels)
{
	for (uint8_t i=0; i<pixels; i++)
	{
		sLED_array_update_pixel(led, startpixel+i);
	}
}


void sLED_array_update_pixel_row(led_config_t *led, uint8_t array, uint8_t row)
{
	for (uint8_t i=0; i<led->array_columns; i++)
	{
			sLED_array_update_pixel(led, (i * led->array_rows) + row + array*led->array_rows*led->array_columns);
	}
}

void sLED_array_update_pixel_column(led_config_t *led, uint8_t array, uint8_t column)
{
	sLED_array_update_pixel_range(led, led->array_rows * column + array*led->array_rows*led->array_columns, led->array_rows);
}

void sLED_array_update_pixel_diagonal_rising(led_config_t *led, uint8_t array, uint8_t diagonal) {
	uint8_t pixels = 0, start = 0;
	if (diagonal < led->array_rows + led->array_columns - 1)
	{
		if (led->array_rows >= led->array_columns)
		{
			if (diagonal < led->array_columns)
			{
				pixels = diagonal;
				start = diagonal;
			}
			else if (diagonal >= led->array_rows)
			{
				pixels = led->array_columns + led->array_rows - diagonal - 2;
				start = led->array_rows + (diagonal - led->array_rows + 1)*led->array_rows - 1;
			}
			else
			{
				pixels = led->array_columns - 1;
				start = diagonal;
			}
		}
		else
		{
			if (diagonal < led->array_rows)
			{
				pixels = diagonal;
				start = diagonal;
			}
			else if (diagonal < led->array_columns)
			{
				pixels = led->array_rows - 1;
				start = led->array_rows + (diagonal - led->array_rows + 1)*led->array_rows - 1;
			}
			else
			{
				pixels = led->array_columns + led->array_rows - diagonal - 2;
				start = led->array_rows + (diagonal - led->array_rows + 1)*led->array_rows - 1;
			}
		}

		start += array*led->array_columns*led->array_rows;
		for (uint8_t i=0; i<=pixels; i++) {
			sLED_array_update_pixel(led, start + i*(led->array_rows - 1));
		}
	}
}

void sLED_array_update_pixel_diagonal_falling(led_config_t *led, uint8_t array, uint8_t diagonal) {
	uint8_t pixels = 0, start = 0;
	if (diagonal < led->array_rows + led->array_columns - 1)
	{
		if (diagonal < led->array_rows) //Invert row count to go from bottom left corner to top right without unintentional jumping.
		{
			diagonal = led->array_rows - diagonal - 1;
		}

		if (led->array_rows >= led->array_columns)
		{
			if (diagonal <= (led->array_rows - led->array_columns))
			{
				pixels = led->array_columns - 1;
				start = diagonal;
			}
			else if (diagonal >= led->array_rows)
			{
				pixels = led->array_columns + led->array_rows - diagonal - 2;
				start = (diagonal - led->array_rows + 1)*led->array_rows;
			}
			else
			{
				pixels = led->array_rows - diagonal - 1;
				start = diagonal;
			}
		}
		else
		{
			if (diagonal >= led->array_columns)
			{
				pixels = led->array_columns + led->array_rows - diagonal - 2;
				start = (diagonal - led->array_rows + 1)*led->array_rows;
			}
			else if (diagonal >= led->array_rows)
			{
				pixels = led->array_rows - 1;
				start = (diagonal - led->array_rows + 1)*led->array_rows;
			}
			else
			{
				pixels = led->array_rows - diagonal - 1;
				start = diagonal;
			}
		}
		
		start += array*led->array_columns*led->array_rows;
		for (uint8_t i=0; i<=pixels; i++) {
			sLED_array_update_pixel(led, start + i*(led->array_rows + 1));
		}
	}
}

void sLED_configure_all(led_config_t *led) {
	sLED_driver_configure_leds(led->array_ptr, led->array_columns*led->array_rows*led->arrays);
}

void sLED_configure_all_oddflip(led_config_t *led) {
	for (uint8_t i = 0; i < led->arrays; i++)
	{
		sLED_driver_configure_leds_2darray_oddflip(led->array_ptr + i*led->array_rows*led->array_columns, led->array_rows, led->array_columns);
	}
}
